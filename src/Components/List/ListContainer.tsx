import { ArrowForward } from "@mui/icons-material";
import * as MD from "@mui/material";
import React from "react";
import { ListContainerProps } from "./Types";

const ListContainer: React.FC<ListContainerProps> = ({
  items,
  handleItemCallback,
}) => {
  return (
    <MD.List
      sx={{
        backgroundColor: "#f6f6f6",
        paddingTop: 0,
        paddingBottom: 0,
      }}
    >
      {/* Display the initial list if there is no navigation history */}
      {items.map((item, index) => (
        <MD.ListItem
          sx={{
            cursor: "pointer",
            "&:hover": {
              backgroundColor: "#cbe9f9",
              "svg ": {
                color: "#282f34",
              },
            },
          }}
          key={index}
          component="div"
          onClick={() => item.subItems && handleItemCallback(item.subItems)}
        >
          <MD.ListItemText primary={item.label} />
          {item.subItems ? (
            <ArrowForward
              sx={{
                color: "#b1b1b1",
              }}
            />
          ) : null}
        </MD.ListItem>
      ))}
    </MD.List>
  );
};

export default ListContainer;
