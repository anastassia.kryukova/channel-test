import React, { useState } from "react";
import * as MD from "@mui/material";
import { ArrowBack, ArrowForward } from "@mui/icons-material";
import { ListControllerProps, ListItemData } from "./Types";
import ListContainer from "./ListContainer";

import "./styles.css"; // Import the CSS styles

const ListController: React.FC<ListControllerProps> = ({ items }) => {
  // State to keep track of the currently displayed list
  const [currentList, setCurrentList] = useState<ListItemData[]>(items);
  // State to maintain navigation history (previous lists)
  const [navHistory, setNavHistory] = useState<ListItemData[][]>([]);
  // States for tracking navigation direction
  const [isTransitioning, setIsTransitioning] = useState(false);
  const [direction, setDirection] = useState<"left" | "right">("left");

  const handleTransitionEnd = () => {
    // Resume the animation
    setIsTransitioning(false);
  };
  // Function to handle item clicks
  const handleItemClick = (subItems: ListItemData[]) => {
    setIsTransitioning(true);
    // Set direction to forward
    setDirection("left");
    // Create a copy of the current navigation history and add the current list to it
    setTimeout(() => {
      const navCurrent = [...navHistory, [...currentList]];
      setNavHistory(navCurrent);
      // Set the current list to the sub-items
      setCurrentList(subItems);
    }, 250);
  };

  // Function to handle going back to the previous list
  const handleBackClick = () => {
    setIsTransitioning(true);
    // Set direction to backward
    setDirection("right");
    setTimeout(() => {
      const navCurrent = [...navHistory];
      // Check if there is any navigation history
      if (navCurrent.length) {
        // Remove the last item from the history and set it as the current list
        const lastItem = navCurrent.pop();
        setCurrentList(lastItem as ListItemData[]);
        // Update the navigation history with the updated history
        setNavHistory(navCurrent);
      }
    }, 250);
  };

  const dispayBackButton = () => {
    if (navHistory.length)
      return (
        <div>
          <MD.IconButton
            onClick={handleBackClick}
            component="div"
            sx={{ display: "flex", justifyContent: "start" }}
            size="small"
            disableRipple
          >
            <ArrowBack
              sx={{
                marginLeft: "8px",
                color: "#b1b1b1",
                "&:hover ": {
                  color: "#282f34",
                },
              }}
            />
          </MD.IconButton>
        </div>
      );
  };

  return (
    <div
      style={{
        margin: 10,
        backgroundColor: "#f6f6f6",
        padding: 0,
        overflow: "hidden",
      }}
    >
      <div
        className={`list-container ${
          isTransitioning
            ? direction === "left"
              ? "slide-enter-left"
              : "slide-enter-right"
            : ""
        }`}
        onAnimationEnd={handleTransitionEnd}
      >
        {dispayBackButton()}
        {/* Display the current list as Material-UI List */}
        <ListContainer
          key={`list-${navHistory.length}`}
          items={currentList}
          handleItemCallback={handleItemClick}
        />
      </div>
    </div>
  );
};

export default ListController;
