// Define the structure of each list item
export interface ListItemData {
  label: string;
  subItems?: ListItemData[];
}

// Define the props for the ListController component
export interface ListControllerProps {
  items: ListItemData[];
}

// Define the props for the ListController component
export interface ListContainerProps {
  items: ListItemData[];
  handleItemCallback: (arg: ListItemData[]) => void;
}
