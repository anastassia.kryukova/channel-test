import "./App.css";
import ListController from "./Components/List/ListController";
import { ListItemData } from "./Components/List/Types";

const list: ListItemData[] = [
  {
    label: "designplox",
  },
  {
    label: "artists",
    subItems: [
      {
        label: "artists-test",
        subItems: [
          {
            label: "artists-test2",
            subItems: [{ label: "artists-test2-test3" }],
          },
        ],
      },
    ],
  },
  {
    label: "albums",
    subItems: [{ label: "albums-test", subItems: [{ label: "albums-test2" }] }],
  },
  {
    label: "in my time of dying",
    subItems: [{ label: "test", subItems: [{ label: "test2" }] }],
  },
];

function App() {
  return (
    <div className="App" style={{ width: "400px" }}>
      <ListController items={list} />
    </div>
  );
}

export default App;
